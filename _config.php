<?php
/**/
use SilverStripe\SiteConfig\SiteConfig;
/* Werkbot Site Config */
SiteConfig::add_extension('WerkbotSiteConfigExtension');
/* Google Analytics */
Page::add_extension('GoogleAnalyticsExtension');
/* Google Tag Manager */
Page::add_extension('GoogleTagManagerExtension');
/* Structured Data */
Page::add_extension('StructuredDataExtension');
/* Open Graph */
Page::add_extension('OpenGraphExtension');
//Article::add_extension('openGraphExtension');
