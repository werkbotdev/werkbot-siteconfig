<?php
use SilverStripe\Control\Director;
use SilverStripe\ORM\DataExtension;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\Requirements;
/**/
class GoogleAnalyticsExtension extends DataExtension {
	/**/
	function contentControllerInit($controller) {
		if (Director::isLive()) {
  		$siteConfig = SiteConfig::current_site_config();
  		$accountId = $siteConfig->GoogleAnalyticsAccountID;
  		if(preg_match("/UA-[0-9]{7,}-[0-9]{1,}/", $accountId)) {
  			Requirements::customScript(<<<JS
  			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  			  ga('create', '$accountId', 'auto');
  			  ga('send', 'pageview');
JS
);
  		}
  	}
	}
}
