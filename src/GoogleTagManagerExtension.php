<?php
use SilverStripe\Control\Director;
use SilverStripe\ORM\DataExtension;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\Requirements;
/**/
class GoogleTagManagerExtension extends DataExtension {
	/**/
	function contentControllerInit($controller) {
		if (Director::isLive()) {
  		$siteConfig = SiteConfig::current_site_config();
  		$accountId = $siteConfig->GoogleTagManagerAccountID;
  		if($accountId){
  			Requirements::customScript(
  			<<<JS
  			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','$accountId');
JS
);
		  }
	   }
	}
}
