<?php
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\TextAreaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
/**/
class OpenGraphExtension extends DataExtension {
  	/**/
  	private static $db = [
  		'OpenGraphTitle' => 'Text',
  		'OpenGraphDescription' => 'Text'
  	];
    /**/
    private static $has_one = [
      'OpenGraphImage' => Image::class
    ];
    /**/
    private static $owns = [
    	'OpenGraphImage'
    ];
  	/**/
  	public function updateCMSFields(FieldList $fields) {

      //MOVE META DATA TO THE SEO TAB
      if($this->owner->hasField("MetaTitle")){
  		    $fields->removeByName('Metadata');
      }

      //UPDATE TAB ORDER/POSITION
      if($this->owner->hasField("MetaTitle")){
  		    $fields->addFieldToTab('Root', new TabSet('SEO', new Tab('Metadata'), new Tab('OpenGraph')));
      }else{
  		    $fields->addFieldToTab('Root', new TabSet('SEO', new Tab('OpenGraph')));
      }

      //METADATA
      if($this->owner->hasField("MetaTitle")){
    		$MetaTitle = TextField::create("MetaTitle", $this->owner->fieldLabel('MetaTitle'))
    			->setDescription("Shown at the top of the browser window and used as the \"linked text\" by search engines.");
    		$fields->addFieldToTab('Root.SEO.Metadata', $MetaTitle);

    		$MetaDescription = TextareaField::create("MetaDescription", $this->owner->fieldLabel('MetaDescription'))
    			->setDescription("Search engines use this content for displaying search results (although it will not influence their ranking).");
    		$fields->addFieldToTab('Root.SEO.Metadata', $MetaDescription);

    		$ExtraMeta = TextareaField::create("ExtraMeta", $this->owner->fieldLabel('ExtraMeta'))
    			->setDescription("HTML tags for additional meta information. For example <meta name=\"customName\" content=\"your custom content here\" />");
    		$fields->addFieldToTab('Root.SEO.Metadata', $ExtraMeta);
      }

  		//OPEN GRAPH - TITLE
  		$OpenGraphTitle = TextField::create('OpenGraphTitle', 'Title')
        ->setAttribute("placeholder", "Enter title")
        ->setDescription("Defaults to Meta Title (if set) then the page title.");
      $fields->addFieldToTab('Root.SEO.OpenGraph', $OpenGraphTitle);

  		//OPEN GRAPH - DESCRIPTION
  		$OpenGraphDescription = TextAreaField::create('OpenGraphDescription', 'Description')
        ->setAttribute("placeholder", "Enter description");
      $fields->addFieldToTab('Root.SEO.OpenGraph', $OpenGraphDescription);

  		//OPEN GRAPH - IMAGE
  		$OpenGraphImage = UploadField::create('OGImage','Image')
  			->setFolderName('OpenGraphImages')
        ->setDescription("Recommended image size:1200 x 630 pixels, minimum image size: 600 x 315 pixels");
  		$OpenGraphImage->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png'));
      $fields->addFieldToTab('Root.SEO.OpenGraph', $OpenGraphImage);

    }
}
