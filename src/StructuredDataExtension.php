<?php
use SilverStripe\Control\Director;
use SilverStripe\ORM\DataExtension;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\Requirements;
/**/
class StructuredDataExtension extends DataExtension {

  /**/
	function contentControllerInit($controller) {

		$SiteConfig = SiteConfig::current_site_config();
		$content = array();

		/* SITE URL */
		$baseurl = Director::AbsoluteBaseURL();
		array_push($content, '"url":"'.$baseurl.'"');

		/* ORGANIZATION NAME */
		if($SiteConfig->Title){
			array_push($content, '"name":"'.$SiteConfig->Title.'"');
		}

		/* LOGO */
		if($SiteConfig->GeneralLogoImage()->exists()){
			$logo_url = $SiteConfig->GeneralLogoImage()->AbsoluteURL;
			array_push($content, '"logo":"'.$logo_url.'"');
		}

		/* SOCIAL PROFILES*/
    if(
			$SiteConfig->SocialFacebookLink ||
      $SiteConfig->SocialTwitterLink ||
      $SiteConfig->SocialLinkedinLink ||
      $SiteConfig->SocialGooglePlusLink ||
      $SiteConfig->SocialInstagramLink ||
      $SiteConfig->SocialPinterestLink ||
      $SiteConfig->SocialYoutubeLink
		){
      $links = array();
      if($SiteConfig->SocialFacebookLink){
        array_push($links, '"'.$SiteConfig->SocialFacebookLink.'"');
      }
      if($SiteConfig->SocialTwitterLink){
        array_push($links, '"'.$SiteConfig->SocialTwitterLink.'"');
      }
      if($SiteConfig->SocialLinkedinLink){
        array_push($links, '"'.$SiteConfig->SocialLinkedinLink.'"');
      }
      if($SiteConfig->SocialGooglePlusLink){
        array_push($links, '"'.$SiteConfig->SocialGooglePlusLink.'"');
      }
      if($SiteConfig->SocialInstagramLink){
        array_push($links, '"'.$SiteConfig->SocialInstagramLink.'"');
      }
      if($SiteConfig->SocialPinterestLink){
        array_push($links, '"'.$SiteConfig->SocialPinterestLink.'"');
      }
      if($SiteConfig->SocialYoutubeLink){
        array_push($links, '"'.$SiteConfig->SocialYoutubeLink.'"');
      }
      $link_string =  implode(",", $links);

			array_push($content, '"sameAs":['.$link_string.']');
		}

		//COMBINE EVERYTHING
    $content_string =  implode(",", $content);

		//OUTPUT TO THE PAGE
		Requirements::insertHeadTags(<<<EOF
<script type="application/ld+json">{"@context":"http://schema.org","@type":"Organization",$content_string}</script>
EOF
);
	}

}
