<?php
/**/
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextAreaField;
use SilverStripe\ORM\DataExtension;
/**/
class WerkbotSiteConfigExtension extends DataExtension {

  /**/
  private static $db = [
    /* GENERAL */
    "GeneralEmail" => "Text",
		"GeneralPhone" => "Text",
		"GeneralFax" => "Text",
		"GeneralAddress" => "Text",

    /* ANALYTICS */
    'GoogleAnalyticsAccountID' => 'Text',
    'GoogleTagManagerAccountID' => 'Text',

    /* SOCIAL */
    'SocialGooglePlusLink' => 'Text',
		"SocialLinkedinLink" => "Text",
		"SocialTwitterLink" => "Text",
		"SocialFacebookLink" => "Text",
		"SocialInstagramLink" => "Text",
		"SocialPinterestLink" => "Text",
		"SocialYoutubeLink" => "Text",

  ];
  /**/
  private static $has_one = [
    'GeneralLogoImage' => Image::class
  ];

  /**/
  public function updateCMSFields(FieldList $fields) {

    /* GENERAL */
    // Email
		$GeneralEmail = TextField::create('GeneralEmail', 'Email')
			->setAttribute('placeholder', 'Enter email address');
    $fields->addFieldsToTab('Root.Main', $GeneralEmail);

    // Phone
		$GeneralPhone = TextField::create('GeneralPhone', 'Phone')
			->setAttribute('placeholder', 'Enter phone number');
    $fields->addFieldsToTab('Root.Main', $GeneralPhone);

    // Fax
		$GeneralFax = TextField::create('GeneralFax', 'Fax')
			->setAttribute('placeholder', 'Enter fax number');
    $fields->addFieldsToTab('Root.Main', $GeneralFax);

    // Address
		$GeneralAddress = TextAreaField::create('GeneralAddress', 'Address')
			->setRows(10);
    $fields->addFieldsToTab('Root.Main', $GeneralAddress);

    // Logo
		$GeneralLogoImage = UploadField::create('GeneralLogoImage','Logo')
      ->setFolderName('LogoImages')
      ->setAllowedFileCategories('image/supported');
    $fields->addFieldsToTab('Root.Main', $GeneralLogoImage);


    /* ANALYTICS */
    $GoogleAnalyticsAccountID = TextField::create('GoogleAnalyticsAccountID', 'Google Analytics Code', '')
      ->setDescription("(Example: UA-XXXXXXXX-X)");
    $fields->addFieldsToTab('Root.Analytics', $GoogleAnalyticsAccountID);

    $GoogleTagManagerAccountID = TextField::create('GoogleTagManagerAccountID', 'Google Tag Manager Code', '')
      ->setDescription("(Example: XXX-XXXXXX)");
    $fields->addFieldsToTab('Root.Analytics', $GoogleTagManagerAccountID);


    /* SOCIAL */
    // Google Plus
		$SocialGooglePlusLink = TextField::create('SocialGooglePlusLink', 'Google Plus')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialGooglePlusLink);

    // Linkedin
		$SocialLinkedinLink = TextField::create('SocialLinkedinLink', 'Linkedin')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialLinkedinLink);

    // Twitter
		$SocialTwitterLink = TextField::create('SocialTwitterLink', 'Twitter')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialTwitterLink);

    // Facebook
		$SocialFacebookLink = TextField::create('SocialFacebookLink', 'Facebook')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialFacebookLink);

    // Instagram
		$SocialInstagramLink = TextField::create('SocialInstagramLink', 'Instagram')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialInstagramLink);

    // Pinterest
		$SocialPinterestLink = TextField::create('SocialPinterestLink', 'Pinterest')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialPinterestLink);

    // Youtube
		$SocialYoutubeLink = TextField::create('SocialYoutubeLink', 'Youtube')
			->setAttribute('placeholder', 'http://www.example.com/');
    $fields->addFieldsToTab('Root.Social', $SocialYoutubeLink);

  }

}
